import React from 'react';
import ReactDOM from 'react-dom/client';
import 'bootstrap/dist/css/bootstrap.min.css';
import App from './App';
// const name = 'Maria Veronica Azaña';
// let job = "Professional Singer"

// let personDisplay1 = (
//         <>
//             <h1>{name}</h1>
//             <h3>{job}</h3>
//         </>
     
//             )

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
    // personDisplay1
    <App />
);

//JSX it is used in rect.js
//Javascipt + XML