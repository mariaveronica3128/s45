import React from 'react'
import {Navbar , Nav} from 'react-bootstrap';
export default function AppNavbar() {
    return (
        <Navbar bg = "dark" expand= "lg" variant = "dark">
            <Navbar.Brand href = "#home">Zuitt Bootcamp</Navbar.Brand>
            <Navbar.Toggle aria-controls="basic-navbar-nav"/>
            <Navbar.Collapse>basic-navbar-nav
                <Nav>
                    <Nav.Link href = "#home">Home</Nav.Link>
                    <Nav.Link href = "#courses">Courses</Nav.Link>
                </Nav>
            </Navbar.Collapse>
        </Navbar>
    )
}

